#!/bin/sh
# get scsi ID assuming disks setup by autocluster
/bin/dd if=/dev/$1 bs=1 count=64 2>/dev/null | head -n 1 | cat -v
