#!/bin/sh
#
# chkconfig: 345 14 88
# description: Runs "multipath" when designated number of SCSI disks available.
#
# Source function library.
. /etc/init.d/functions

# Note that you should not run *chkconfig* to enable this initscript
# on nodes where you do not expect to see autocluster iSCSI devices.
# Doing so will merely annoy you by slowing down system boot by
# waiting for phantom devices to appear.

PATH=/sbin:/bin:/usr/sbin:/usr/bin

RETVAL=0

npaths=$((@@SHAREDDISK_COUNT@@ * @@SHARED_DISK_MULTIPATH_NUMPATHS@@))

correct_number_of_devices ()
{
    _glob="$1"
    _num_desired=$2

    _n=$(grep AUTCLSTR $_glob 2>/dev/null | wc -l)
    
    [ "$_n" = "$_num_desired" ]
}

wait_for_devices ()
{
    _glob="$1"
    _num_desired=$2
    _timeout="${3:-60}"

    _count=0
    while : ; do
	if correct_number_of_devices "$_glob" $_num_desired ; then
	    return 0
	fi

	if [ $_count -ge $_timeout ] ; then
	    echo "TIMEOUT (iscsi)"
	    return 1
	fi

        _count=$(($_count + 1))
        sleep 1
    done
}

sd_pat='/sys/block/sd*/device/vendor'
slave_pat='/sys/block/dm-*/slaves/*/device/vendor'

start()
{
    echo -n "Setting up multipath for iSCSI devices:"

    if wait_for_devices "$sd_pat" $npaths ; then
	_c=0
	while : ; do
	    multipath
	    if correct_number_of_devices "$slave_pat" $npaths ; then
		break
	    fi
	    if [ $_c -ge 30 ] ; then
		echo "TIMEOUT (multipath)"
		failure
		RETVAL=1
		break
	    fi
	    multipath -F
	    _c=$(($_c + 1))
	    sleep 1
	done

	success
        echo
    else
	RETVAL=1
	failure
    fi
}

stop()
{
    echo -n "Flushing multipath devices:"

    if multipath -F && \
	wait_for_devices "$slave_pat"  0 ; then
        success
    else
	RETVAL=1
	failure
    fi
}

case "$1" in
        start)
                start
                ;;
        stop)
                stop
                ;;
        restart)
                stop
                start
                ;;
        *)
                echo $"Usage: $0 {start|stop|restart}"
                exit 1
esac
exit $RETVAL
