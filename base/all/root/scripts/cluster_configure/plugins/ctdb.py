# Configure clustered Samba nodes

# Module for handling addresses.

# Copyright (C) Amitay Isaacs 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import logging

import package

def gen_public_addresses(config, pkg, req_ips):
    """Given a config object, return the formatted public addresses."""

    stack = package.get(config, pkg, "network_stack")
    out = []
    out6 = []
    foo = package.get(config, pkg, "public_addresses")
    for tmp in foo.split():
        (startip, num_addrs, iface) = tmp.split(',')
        (a,b,c,d) = startip.split('.')
        for i in xrange(int(num_addrs)):
            octet = int(d) + i
            if stack == 'ipv4' and req_ips == 'default':
                out.append('%s.%s.%s.%d/24 %s\n' % (a, b, c, octet, iface))
            elif stack == 'ipv6' and req_ips == 'default':
                out6.append('fc00:%s:%s:%s::%s/64 %s\n' % (a, b, c, octet, iface))
            elif stack == 'dual':
                if req_ips in ['ipv4', 'dual', 'default']:
                    out.append('%s.%s.%s.%d/24 %s\n' % (a, b, c, octet, iface))
                if req_ips in ['ipv6', 'dual', 'default']:
                    out6.append('fc00:%s:%s:%s::%s/64 %s\n' % (a, b, c, octet, iface))

    # Special case: if the file would be empty then it should be there...
    ret = "".join(out) + "".join(out6)
    if ret:
        return ret
    else:
        return None
