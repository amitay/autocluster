# Configure clustered Samba nodes

# Module for handling package configuration.

# Copyright (C) Martin Schwenke 2010

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import util

_package_prefix = "package"

def names(config):
    """Given a config, return the names of all packages."""

    return util.names(config, _package_prefix)

def get(config, package, key):
    """Given a config, return the the value of specify key
        for the package."""

    return config.get(_package_prefix + ":" + package, key)

def retrieve(config, package):
    """Given a config, retrieve all the configuration information
    about package.  The result is a dictionary representing the
    package's configuration file section."""

    return util.retrieve(config, _package_prefix + ":" + package)

