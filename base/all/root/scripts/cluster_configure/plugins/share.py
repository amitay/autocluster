# Configure clustered Samba nodes

# Module for handling shares.

# Copyright (C) Martin Schwenke 2010

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import util

_share_prefix = "share"

def names(config):
    """Given a config, return the names of all shares."""

    return util.names(config, _share_prefix)

def retrieve(config):
    """Given a config, retrieve all the information about shares.  The
    result is a dictionary with share names (minus "share:") as keys
    where each value is a dictionary for the section."""

    return util.retrieve(config, _share_prefix)

def format(config, package, format, items):
    """Given a config object, return the formatted shares.  Use format
    string and apply it to the share fields in the order specified in
    items.  Argument package is ignored but is present for consistency."""

    shares = retrieve(config)

    out = []

    for e in shares.keys():
        # Add name.
        shares[e]["name"] = e

        def get_val(k):
            ret = shares[e].get(k)
            if ret is None:
                raise ValueError("No option '%s'" % k)
            return ret

        out.append(format % tuple(map(get_val, items)))

    return "".join(out)
