# Configure clustered Samba nodes

# Module for handling exports.

# Copyright (C) Martin Schwenke 2010

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import logging

import util
import share
import package

_export_prefix = "export"

def names(config):
    """Given a config, return the names of all exports."""

    return util.names(config, _export_prefix)

def retrieve(config, package):
    """Given a config, retrieve all the information about exports for
    package.  The result is a dictionary with export names as keys
    where each value is a dictionary for the section."""

    return util.retrieve(config, _export_prefix + ":" + package)

def _check(config, package):
    """Given a config sanity check the exports for the package."""

    exports = retrieve(config, package)
    shares = share.retrieve(config)

    for e in exports.keys():
        s = exports[e].get("share", e)
        if not s in shares:
            raise KeyError, \
                "export:%s:%s requires share:%s" % (package, e, s)

def check(config):
    """Given a config sanity check all of the exports."""

    logger = logging.getLogger("cluster-configure")

    status = True

    for p in package.names(config):
        try:
            _check(config, p)
        except KeyError, str:
            logger.error("configuration error: %s", str)
            status= False

    return status

def format(config, package, format, items):
    """Given a config object, return the formatted exports for
    package.  Use format string and apply it to the export fields in
    the order specified in items.  Before formatting, the dictionary
    for each export is augmented with the options for the
    corresponding share.  If there's a collision in an option name
    then it is prefixed with "share:"."""

    # Get export and share info.
    exports = retrieve(config, package)
    shares = share.retrieve(config)

    out = []

    for e in exports.keys():
        # Add name.
        exports[e]["name"] = e

        # Add share config.
        s = exports[e].get("share", e)
        for (k,v) in shares[s].iteritems():
            if k in exports[e]:
                exports[e]["share:" + k] = v
            else:
                exports[e][k] = v

        def get_val(k):
            ret = exports[e].get(k)
            if ret is None:
                raise ValueError("No option '%s' for package:%s" % (k, package))
            return ret

        out.append(format % tuple(map(get_val, items)))

    return "".join(out)
