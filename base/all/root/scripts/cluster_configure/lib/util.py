# Configure clustered Samba nodes

# Utility functions.

# Copyright (C) Martin Schwenke 2010

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

def names(config, prefix):
    """Given a config object, return the names sections starting
    with "prefix:" but strip that from each one."""

    t = prefix + ":"

    return map(lambda y: str.replace(y, t, "", 1),
               filter(lambda x: str.find(x, t) == 0,
                      config.sections()))

def retrieve(config, prefix):
    """Given a config, retrieve all the information about shares.  The
    result is a dictionary with share names (minus "<prefix>:") as
    keys where each value is a dictionary for the section."""

    return dict(map(lambda s: (s, dict(config.items(prefix + ":" + s))),
                    names(config, prefix)))

