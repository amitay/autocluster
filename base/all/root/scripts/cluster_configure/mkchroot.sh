#!/bin/sh

set -e

# Make a simple chroot for vsftpd and scp.

# Note that for the base chroot this attempts to use read-only bind
# mounts.  These will be silently readable on older kernels.  If
# you're using this for anything except simple testing then PLEASE
# TEST the chroot's security!

usage ()
{
    cat <<EOF
usage: $0 { create | destroy } chroot_dir
       $0 mount src_dir dst_dir
       $0 umount dst_dir
EOF
    exit 1
}

[ $# -ge 1 ] || usage

action="$1"
shift
case "$action" in
    create|destroy|umount)
	:
	;;
    mount)
	[ $# -ge 2 ] || usage
	;;
    *)
	usage
esac

do_mount ()
{
    _src="$1"
    _dst="$2"

    if [ -f "$_dst" ] ; then
	return 0
    fi

    if [ -f "$_src" ] ; then
	mkdir -vp $(dirname "$_dst")
	touch "$_dst"
    else
	mkdir -vp "$_dst"
    fi

    mount -v --bind "$_src" "$_dst"
}

do_umount ()
{
    _dst="$1"

    _out=$(awk "\$2 == \"$_dst\" {print \$2}" /proc/mounts)    
    if [ -n "$_out" ] ; then
	_is_file=false
	if [ -f "$_dst" ] ; then
	    _is_file=true
	fi
	umount -v "$_dst"
	if $_is_file ; then
	    rm -vf "$_dst"
	    rmdir --ignore-fail-on-non-empty -vp $(dirname "$_dst")
	else
	    rmdir --ignore-fail-on-non-empty -vp "$_dst"
	fi
    fi
}

first_ro_mount=true
can_remount_ro=true

do_mount_ro ()
{
    _src="$1"
    _dst="$2"

    if [ -f "$_dst" ] ; then
	return 0
    fi

    do_mount "$_src" "$_dst"

    if $first_ro_mount ; then
	if ! mount -v -o remount,ro "$_dst"; then
	    cat <<EOF
Unable to remount $_dst read-only.  Won't try this again.

WARNING: Your chroot may be less secure than you think!

EOF
	    can_remount_ro=false
	else
	    if f=$(mktemp -p "$_dst" >/dev/null 2>&1) ; then
		rm -f "$f"
		cat <<EOF
WARNING: Bind mounts don't really look to be read-only!
EOF
	    fi
	fi
    else
	if $can_remount_ro ; then
	    mount -v -o remount,ro "$_dst"
	fi
    fi

    first_ro_mount=false
}

sftp_server=""
for d in /usr/libexec/openssh /usr/lib/openssh ; do
    f="$d/sftp-server"
    if [ -x "$f" ] ; then
	sftp_server="$f"
	break
    fi
done
if [ -z "$sftp_server" ] ; then
    echo "$0: error - could not find location of sftp_server"
    exit 2
fi

case $(uname -m) in
    x86_64)
	lib="lib64"
	;;
    *)
	lib="lib"
esac

mounts="\
/usr/$lib \
/$lib \
/etc/nsswitch.conf \
/etc/resolv.conf \
$sftp_server \
/usr/bin/scp \
"

do_create ()
{
    chroot_dir="$1"

    mkdir -p "$chroot_dir"

    fake="${chroot_dir}/.mkchroot"
    touch "$fake"
    ls -l "$fake"

    d="$chroot_dir/dev"
    mkdir -vp "$d"
    cp -va /dev/null /dev/zero "$d/"

    for s in $mounts ; do
	f="$chroot_dir$s"
	do_mount_ro "$s" "$f"
    done

    d="$chroot_dir/etc/passwd"
    touch "$d"
    ls -l "$d"
}

do_destroy ()
{
    chroot_dir="$1"

    rm -vf "$chroot_dir/etc/passwd"

    for i in "null" "zero" ; do
	rm -vf "$chroot_dir/dev/$i"
    done
    rmdir -v "$chroot_dir/dev"

    # Reverse the list 1st.
    rmounts=
    for s in $mounts ; do
	rmounts="$s${rmounts:+ }${rmounts}"
    done

    for s in $rmounts ; do
	do_umount "${chroot_dir}$s"
    done

    fake="${chroot_dir}/.mkchroot"
    rm -vf "$fake"
    rmdir -v "$chroot_dir"
}

"do_$action" "$@"
