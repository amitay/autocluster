#!/bin/sh

#config: CLUSTERFS_TYPE

set -e

stage="$1" ; shift

prefix="$stage"
case "$stage" in
    install_packages)
	prefix="install"
	# Always do the extra packages task
	set -- "extra_packages" "$@"
	;;
esac

dn=$(dirname $0)

config="${dn}/cluster_setup.config"
if [ ! -r "$config" ] ; then
    echo "Can not local configuration file \"${config}\""
fi
. "$config"

for task ; do
    case "$task" in
	clusterfs)
	    type="$CLUSTERFS_TYPE"
	    file="${prefix%_clusterfs}_clusterfs_${type}.sh"
	    ;;
	*)
	    file="${prefix}_${task}.sh"
    esac

    path="${dn}/tasks/${file}"

    if [ ! -r "$path" ] ; then
	echo "Unable to find script \"${file}\" for stage \"${stage}\" task \"${task}\""
	exit 1
    fi

    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    printf "%% %-66s %%\n" "Stage \"${stage}\", task \"${task}\" on host ${HOSTNAME%%.*}"
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    set -- # pass no arguments
    . "$path"
done
