#!/bin/bash

# catch errors
set -e
set -E
trap 'es=$?; 
      echo ERROR: failed in function \"${FUNCNAME}\" at line ${LINENO} of ${BASH_SOURCE[0]} with code $es; 
      exit $es' ERR


# Setup things up so that TSM_SERVER is the first tsm_server node and
# any nas nodes are listed in the TSM server configuration as
# clients.
NODES="@@NODES@@"
TSM_SERVER=""
TSM_CLIENTS=""

for n in $NODES ; do
    case "$n" in
	nas:*)
	    t="${n%:*}"
	    TSM_CLIENTS="${TSM_CLIENTS}${TSM_CLIENTS:+ }${t##*:}"
	    ;;
	tsm_server:*)
	    [ -n "$TSM_SERVER" ] && continue
	    t="${n%:*}"
	    TSM_SERVER="${t##*:}"
    esac
done

if [ -z "$TSM_SERVER" ] ; then
    echo "ERROR: No tsm_server nodes in this cluster!"
    exit 1
fi

if [ -z "$TSM_CLIENTS" ] ; then
    echo "ERROR: No nas nodes in this cluster!"
    exit 1
fi

yum -y install TIVsm-BA TIVsm-HSM TIVsm-API TIVsm-server

echo "Ensuring multipath is disabled on this disk"
chkconfig multipathd off || true
service multipathd stop || true
multipath -F || true
cat <<EOF >> /etc/multipath.conf
blacklist {
    "^@@SYSTEM_DISK_PREFIX@@b"
}
EOF

echo "Disabling multipath at boot"
sed -e 's/rhgb/rhgb nompath/g' -i.orig /boot/grub/grub.conf

mkfs.ext3 -F /dev/@@SYSTEM_DISK_PREFIX@@b
mkdir -p /tsmdiskpool
cat <<EOF >> /etc/fstab
/dev/@@SYSTEM_DISK_PREFIX@@b /tsmdiskpool  ext3    defaults        1 1
EOF
mount /tsmdiskpool

ln -sf /opt/tivoli/tsm/server/bin/dsmserv.rc /etc/init.d/dsmserv
ln -sf /etc/init.d/dsmserv /etc/rc3.d/S99dsmserv
/etc/init.d/dsmserv start

echo "Setting up dsm.sys"
cat <<EOF > /opt/tivoli/tsm/client/ba/bin/dsm.sys
SErvername  $TSM_SERVER
    COMMMethod         TCPip
    TCPPort            1500
    TCPServeraddress   $TSM_SERVER
    ASNODENAME         @@TSMNAME@@
    PASSWORDACCESS     generate
    EXCLUDE.DIR        /.../.snapshots
    ERRORLOGName       /var/log/dsmerror.log
EOF

echo "Setting up dsm.opt"
cat <<EOF > /opt/tivoli/tsm/client/ba/bin/dsm.opt
servername $TSM_SERVER
dateformat 2
EOF



{
    cat <<EOF
copy domain STANDARD ALLTODISK
upd mg ALLTODISK STANDARD STANDARD migrequiresbkup=no SPACEMGTECHnique=selective
activate pol ALLTODISK STANDARD
register node @@TSMNAME@@ admin domain=ALLTODISK userid=none backdel=yes
EOF

    AGENTS=""
    for c in $TSM_CLIENTS ; do
	echo "register node ${c}.@@DOMAIN@@ admin domain=ALLTODISK userid=none backdel=yes"
	AGENTS="${AGENTS}${AGENTS:+,}${c}.@@DOMAIN@@"
    done

    cat <<EOF
grant proxynode target=@@TSMNAME@@ agent=$AGENTS
def dbvol /tsmdiskpool/db1.dsm format=@@TSM_DB_SIZE@@ wait=yes
del dbv /opt/tivoli/tsm/server/bin/db.dsm wait=yes
extend db `expr @@TSM_DB_SIZE@@ - 16` wait=yes
set logmode rollf
def vol spacemgpool /tsmdiskpool/vol01.dsm format=@@TSM_SPACE_MGMT_SIZE@@ wait=yes
def vol backuppool  /tsmdiskpool/backup1.dsm format=@@TSM_BACKUP_POOL_SIZE@@ wait=yes
del vol /opt/tivoli/tsm/server/bin/backup.dsm wait=yes
def vol archivepool /tsmdiskpool/archive1.dsm format=@@TSM_ARCHIVE_POOL_SIZE@@ wait=yes
del vol /opt/tivoli/tsm/server/bin/archive.dsm wait=yes
upd stgpool archivepool next=spacemgpool
upd stgpool backuppool  next=spacemgpool
EOF
} | dsmadmc -id=admin -password=admin -noconfirm || true

echo "TSM server setup complete"
exit 0
