#!/bin/bash

# Per-node NAS cluster configuration steps

set -e

conf_file="/root/scripts/nas.conf"

auth_type=$(sed -r -n -e 's@^auth_method[[:space:]]*=[[:space:]]*(files|winbind)[[:space:]]*$@\1@p' "$conf_file")

case "$auth_type" in
    winbind|files) : ;;
    *)
	echo "Unsupported authentication type \"${auth_type}\""
	usage
esac

domain=$(sed -r -n -e 's@^domain[[:space:]]*=[[:space:]]*([^ ]*)[[:space:]]*$@\1@p' "$conf_file" | tr '[a-z]' '[A-Z]')

kdc=$(sed -r -n -e 's@^kdc[[:space:]]*=[[:space:]]*([^ ]*)[[:space:]]*$@\1@p' "$conf_file")

case "$auth_type" in
    winbind)
	echo "Setting up NSS, PAM, KRB5..."
	authconfig --update --nostart \
	    --enablewinbindauth --enablewinbind \
	    --disablekrb5 --krb5kdc=$kdc.$domain  --krb5realm=$domain
	;;
    files)
	echo "Setting up NSS, PAM, KRB5..."
	authconfig --update --nostart \
	    --disablewinbindauth --disablewinbind \
	    --disablekrb5 --krb5kdc=$kdc.$domain  --krb5realm=$domain
	;;
esac

echo "Configuring services..."
(cd scripts/cluster_configure && \
    ./cluster-configure.py -t "templates/rhel/" -vv "$conf_file")
