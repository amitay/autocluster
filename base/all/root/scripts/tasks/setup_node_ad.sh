#!/bin/sh

#config: AD_ADMIN_PASS AD_DNS_FORWARDER AD_FUNCTION_LEVEL AD_NETBIOS_NAME
#config: DOMAIN IPNUM NETWORK_PRIVATE_PREFIX WORKGROUP

set -e

# Remove samba3 configuration file
rm -f /etc/samba/smb.conf

# Create AD instance
echo "Provisioning Samba4 AD domain: ${DOMAIN}"
hostip="${NETWORK_PRIVATE_PREFIX}.${IPNUM}"
samba-tool domain provision \
    --realm="$DOMAIN" \
	--domain="$WORKGROUP" \
	--host-ip="$hostip" \
	--host-name="$AD_NETBIOS_NAME" \
	--server-role="domain controller" \
	--function-level="$AD_FUNCTION_LEVEL" \
	--dns-backend="SAMBA_INTERNAL"

# Add samba configuration parameters
sed -i -e '/server services/a\
        allow dns updates = True\
        nsupdate command = nsupdate\
        dns forwarder = ${AD_DNS_FORWARDER}\
        dns recursive queries = Yes' /etc/samba/smb.conf

# Add users/groups
echo "Adding users and groups"
samba-tool domain passwordsettings set --min-pwd-length=3
samba-tool domain passwordsettings set --complexity=off
samba-tool user setexpiry --noexpiry Administrator
samba-tool user setpassword administrator --newpassword="$AD_ADMIN_PASS"
samba-tool group add group1
samba-tool group add group2
samba-tool user add user1 "$AD_ADMIN_PASS"
samba-tool group addmembers group1 user1
samba-tool user setexpiry --noexpiry user1
samba-tool user add user2 "$AD_ADMIN_PASS"
samba-tool group addmembers group2 user2
samba-tool user setexpiry --noexpiry user2

# Samba start-up
cat >> /etc/rc.d/rc.local <<EOF

# Start SAMBA AD server
/usr/sbin/samba
EOF
mkdir /var/run/samba

# Add NetBIOS name to hosts
echo "${hostip} ${AD_NETBIOS_NAME}.${DOMAIN} ${AD_NETBIOS_NAME}" >>/etc/hosts

# Update DNS configuration
echo "Updating /etc/resolv.conf"
sed -i -e 's@^\(nameserver.*\)@#\1@' /etc/resolv.conf
echo "nameserver $hostip" >> /etc/resolv.conf

# Start SAMBA
/usr/sbin/samba
