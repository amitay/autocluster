#!/bin/sh

set -e

echo "Installing Samba4 packages"
yum -y install samba4-dc samba4-python samba4-winbind
