#!/bin/sh

#config: CONFIG_EXTRA_PACKAGES

echo "Updating from YUM repositories"
yum -y update

extra_packages="$CONFIG_EXTRA_PACKAGES"
if [ -n "$extra_packages" ] ; then
    echo "Installing extra packages"
    yum -y install $extra_packages
fi
