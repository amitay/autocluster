#!/bin/sh

#config: GPFS_BASE_VERSION RHEL_ARCH

# Must force the base GPFS package first, due to the way GPFS updates
# work.
echo "Installing GPFS base"
yum -y install "gpfs.base-${GPFS_BASE_VERSION}.${RHEL_ARCH}"

echo "Updating GPFS base"
yum -y update

echo "Installing remaining GPFS packages"
yum -y install gpfs.docs gpfs.gpl gpfs.msg.en_US gpfs.gskit

echo "Adding GPFS's bin to PATH"
cat >/etc/profile.d/gpfs-autocluster.sh <<EOF
# Added by autocluster
pathmunge /usr/lpp/mmfs/bin
EOF

echo "Rebuilding the GPFS modules"
(cd /usr/lpp/mmfs/src && make Autoconfig World InstallImages)
