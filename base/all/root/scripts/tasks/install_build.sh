#!/bin/sh

# Setup RPM development
echo "Installing RPM developing and setting up rpm build"
yum -y install rpmdevtools

# Install the performance co-pilot packages to allow building of the
# CTDB pmda code.  It is OK if this fails (these packages are only
# available in EPEL) since this script will continue...
echo "Installing PCP development libraries - this may fail"
yum -y install pcp-libs pcp-libs-devel || true

# Packages required for samba development
echo "Installing Samba build dependencies"
yum -y install readline-devel libacl-devel e2fsprogs-devel \
    libxslt docbook-utils docbook-style-xsl dbus-devel \
    libaio-devel libcap-devel quota-devel \
    perl-Parse-Yapp perl-ExtUtils-MakeMaker

exit 0
