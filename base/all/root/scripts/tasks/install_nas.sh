#!/bin/sh

echo "Installing tdb tools packages"
yum -y install tdb-tools

echo "Installing ctdb packages"
yum -y install ctdb ctdb-debuginfo ctdb-devel ctdb-tests

echo "Installing samba packages"
yum -y install \
    samba samba-client samba-doc \
    samba-winbind samba-winbind-clients
yum -y install samba-vfs-gpfs || true

echo "Installing rssh"
yum -y install rssh
