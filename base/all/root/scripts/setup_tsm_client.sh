#!/bin/bash

# catch errors
set -e
set -E
trap 'es=$?; 
      echo ERROR: failed in function \"${FUNCNAME}\" at line ${LINENO} of ${BASH_SOURCE[0]} with code $es; 
      exit $es' ERR

# Setup things up so that TSM_SERVER is the first tsm_server node and
# any nas nodes are listed in the TSM server configuration as clients.
NODES="@@NODES@@"
TSM_SERVER=""
TSM_CLIENTS=""

for n in $NODES ; do
    case "$n" in
	nas:*)
	    t="${n%:*}"
	    # comma separated
	    TSM_CLIENTS="${TSM_CLIENTS}${TSM_CLIENTS:+,}${t##*:}"
	    ;;
	tsm_server:*)
	    [ -n "$TSM_SERVER" ] && continue
	    t="${n%:*}"
	    TSM_SERVER="${t##*:}"
    esac
done

if [ -z "$TSM_SERVER" ] ; then
    echo "ERROR: No tsm_server nodes in this cluster!"
    exit 1
fi

################################

mmgetstate | grep active > /dev/null || {
    echo "GPFS must be running to setup TSM client"
    exit 1
}

################################
# wait for GPFS to become active
gpfs_wait_active() {
    echo "Waiting for gpfs to become active"
    while ! mmgetstate | grep active > /dev/null; do 
	echo -n "."; 
	sleep 1; 
    done
    echo
}

########################
# wait for GPFS to mount
gpfs_wait_mount() {
    gpfs_wait_active
    echo "Waiting for gpfs to mount (can take a while)"
    while ! mount | grep /dev/gpfs0 > /dev/null; do 
	sleep 2; 
	mmmount gpfs0 -a 2> /dev/null || true
    done
}


######################################
# run the given command on all of the $TSM_CLIENTS 
run_on_client_nodes() {
    onnode -n -qp "$TSM_CLIENTS" "$@"
}

# install the SoFS toolset first so that gpfsready is put into place
# and the HSM packages will not touch it any more
run_on_client_nodes yum -y install sofs-toolset
run_on_client_nodes yum -y install TIVsm-BA TIVsm-HSM TIVsm-API expect

echo "Setting up dsm.sys"
cat <<EOF > /opt/tivoli/tsm/client/ba/bin/dsm.sys
SErvername  $TSM_SERVER
    COMMMethod         TCPip
    TCPPort            1500
    TCPServeraddress   $TSM_SERVER
    PASSWORDACCESS     generate
    EXCLUDE.DIR        /.../.snapshots
    EXCLUDE            /.../.snapshots/*
    EXCLUDE.DIR        /.../.SpaceMan
    EXCLUDE            /.../.SpaceMan/*
    EXCLUDE.DIR        /.../.ctdb
    EXCLUDE            /.../.ctdb/*
    ERRORLOGName       /var/log/dsmerror.log
    ASNODENAME         @@TSMNAME@@
EOF
run_on_client_nodes rsync $HOSTNAME:/opt/tivoli/tsm/client/ba/bin/dsm.sys /opt/tivoli/tsm/client/ba/bin

echo "Setting up dsm.opt"
cat <<EOF > /opt/tivoli/tsm/client/ba/bin/dsm.opt
servername $TSM_SERVER
dateformat 2
EOF
run_on_client_nodes rsync $HOSTNAME:/opt/tivoli/tsm/client/ba/bin/dsm.opt /opt/tivoli/tsm/client/ba/bin

run_on_client_nodes "preprpnode `hostname -s`"
mkrpdomain -F /etc/ctdb/nodes SoFS

echo "Setting TSM password"
cat <<EOF > /tmp/tsmpwd
spawn dsmsetpw
expect "enter password"
send "admin
"
sleep 2
EOF
run_on_client_nodes "rsync $HOSTNAME:/tmp/tsmpwd /tmp/; expect /tmp/tsmpwd"
run_on_client_nodes rm -f /tmp/tsmpwd

# we have to use a rather torturous procedure ...
echo "Killing ctdb"
run_on_client_nodes killall ctdbd || true

echo "temporarily disabling automount of gpfs0"
mmchfs gpfs0 -A no

echo "Shutting down gpfs"
mmshutdown -a

echo "Starting up gpfs again"
mmstartup -a

gpfs_wait_active

echo "Enabling DMAPI in gpfs"
mmchfs gpfs0 -z yes

echo "Enabling LOWSPACE events in GPFS"
mmchconfig enableLowspaceEvents=yes

echo "enabling automount of gpfs0"
mmchfs gpfs0 -A yes

echo "Restarting GPFS"
mmshutdown -a
mmstartup -a

gpfs_wait_mount

echo "Adding space management to /gpfs"
dsmmigfs add /gpfs -LT=100 -HT=100

echo "Enabling failover"
run_on_client_nodes dsmmigfs ENABLEFailover

echo "Restarting gpfs"
mmshutdown -a
mmstartup -a

gpfs_wait_mount

cat <<EOF
TSM client setup complete on $TSM_CLIENTS
You should reboot the nodes before using HSM
EOF
