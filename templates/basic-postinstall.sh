##################################################
# BEGIN basic-postinstall.sh
#
[ "@@WEBPROXY@@" = "" ] || {
  http_proxy=@@WEBPROXY@@
  export http_proxy
}

echo "Using web proxy: \$http_proxy"

# Force up the network, as kickstart may not have started it.  Modern
# RHEL/Fedora renames network interfaces to "well-known" names that
# are hard to know in this script.  So find the 1st ethernet interface
# and use it.
rm -f /etc/sysconfig/network-scripts/ifcfg-eth*
rm -f /etc/sysconfig/network-scripts/ifcfg-en*

dev=$(ip link show |
    awk '/^[[:digit:]]/ { dev=gensub(/:/, "", "", $2) } \\
	 /^[[:space:]]*link\\/ether/ { print dev ; exit}')
echo "Forcing up network interface \"$dev\""

ip link set $dev up
ip addr add @@INSTALL_IP@@ dev $dev
if [ -n "@@INSTALL_GW@@" ] ; then
    ip route add default via @@INSTALL_GW@@
fi
ip addr show
ip route show

cat << EOF > /etc/resolv.conf
domain @@DOMAIN@@
search @@DNSSEARCH@@
nameserver @@NAMESERVER@@
EOF

echo "Setting up YUM repositories" 

cat << EOF > /etc/yum.repos.d/autocluster.repo
@@@YUM_TEMPLATE@@@
EOF

# CentOS has some weird plugins that slow thing down
echo "Switching off YUM plugins"

sed -i -e 's@^plugins=1@plugins=0@' /etc/yum.conf

echo "Updating from YUM repositories"
yum clean all
yum -y update
# Leave things in a state that forces any nodes based on this base
# image fetch all of the repo data next time an update is done.
yum clean all

# This stops NICs with fake MAC addresses being carried forward from
# the base install.
rm -f /etc/udev/rules.d/70-persistent-net.rules

sync
#
# END basic-postinstall.sh
##################################################
