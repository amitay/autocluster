#!/bin/bash

dry_run=false
if [ "$1" = "-n" ] ; then
    dry_run=true
    shift
fi

if [ $# -ne 1 ]; then
    echo "Usage: $0 [ -n ] <autocluster-config>"
    exit 1
fi

die ()
{
    echo "ERROR: $*" >&2
    exit 1
}

gen_xml ()
{
    local netname="$1"
    local ip="$2"
    local ipv6="$3"
    local opts="$4"

    local uuid=$(uuidgen)

    cat <<EOF
<network>
  <name>${netname}</name>
  <uuid>${uuid}</uuid>
EOF

    local o bridge_iface
    bridge_iface=""
    for o in $opts ; do
	case "$o" in
	    bridge\=*)
		bridge_iface="${o#bridge=}"
	esac
    done

    if [ -z "$bridge_iface" ] ; then
	cat <<EOF
  <forward mode='nat'/>
EOF
    else
	cat <<EOF
  <forward dev='${bridge_iface}' mode='route'>
    <interface dev='${bridge_iface}'/>
  </forward>
EOF
    fi

    ip_addr=${ip%/*}
    ip_mask=${ip#*/}

    ipv6_addr=${ipv6%/*}
    ipv6_mask=${ipv6#*/}

    cat <<EOF
  <bridge name='${netname}' stp='on' forwardDelay='0' />
  <ip address='${ip_addr}' prefix='${ip_mask}' />
  <ip family='ipv6' address='${ipv6_addr}' prefix='${ipv6_mask}' />
</network>
EOF
}

base_dir=$(cd -P $(dirname $0) ; cd .. ; echo $PWD)
autocluster="$base_dir/autocluster"
if [ ! -x "$autocluster" ]; then
    autocluster="autocluster"
fi

# Run autocluster to determine desired network configuration.
network_map=$(NAME=setup_networks IPNUM=1 \
    "$autocluster" "${1:+-c}" "$1" \
    -e 'make_network_map ; echo $network_map' 2>/dev/null)

if [ $? -ne 0 -o -z "$network_map" ]; then
    die "autocluster command failed to generate network list"
fi


while read netname dev ip ipv6 mac opts ; do
    echo "Setting up network \"${netname}\""
    t=$(mktemp)
    gen_xml "$netname" "$ip" "$ipv6" "$opts" >"$t"
    if $dry_run ; then
	cat "$t"
    else
	virsh net-define "$t" && \
	    virsh net-start "$netname" && \
	    virsh net-autostart "$netname"
    fi
    rm -f "$t"
done <"$network_map"
