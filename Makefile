version=$$(git describe | sed -e 's@autocluster-@@' -e 's@-@_@g')

prefix	= /usr/local
datadir	= ${prefix}/share/autocluster
bindir	= ${prefix}/bin
DESTDIR	=

datas	= base config.d host_setup templates
hacks	= autocluster.hack
genpkg	= debian/changelog autocluster.spec ChangeLog

all:	$(hacks)

autocluster.hack:	autocluster
	sed -e '/##BEGIN-INSTALLDIR-MAGIC##/i \
installdir=$(datadir)' -e '/##BEGIN-INSTALLDIR-MAGIC##/,/##END-INSTALLDIR-MAGIC##/d' $< > $@

install:	all
	mkdir -p $(DESTDIR)$(datadir)
	cp -a $(datas) $(DESTDIR)$(datadir)/
	mkdir -p $(DESTDIR)$(bindir)
	install -m 755 vircmd $(DESTDIR)$(bindir) 
	install -m 755 autocluster.hack $(DESTDIR)$(bindir)/autocluster

debian/changelog:	debian/changelog.in Makefile .git/refs/heads/master
	sed -e "s/@@DATE@@/$$(date '+%a, %e %b %Y %T %z')/" -e "s/@@VERSION@@/$(version)/" $< > $@ || rm -f $@

autocluster.spec:	autocluster.spec.in Makefile .git/refs/heads/master
	sed -e "s/@@VERSION@@/$(version)/" $< > $@ || rm -f $@

ChangeLog:	.git/refs/heads/master
		git log > $@ || rm -f $@

dist:	$(genpkg)
	t=autocluster-$(version) && \
	git archive --prefix=$${t}/ HEAD > $${t}.tar && \
	tar r --transform s@^@$${t}/@ -f $${t}.tar $(genpkg) && \
	gzip -f $${t}.tar

rpm:	dist
	rpmbuild -tb autocluster-$(version).tar.gz

clean:
	rm -f $(hacks) $(genpkg)
