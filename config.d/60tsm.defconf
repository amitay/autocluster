# Hey Emacs, this is a -*- shell-script -*- !!!

defconf TSMDISKSIZE "50G" \
	"<n>G" "size of the TSM storage disk"

defconf TSMNAME "TSM01" \
	"<name>" "name used by nodes to talk to TSM server"

defconf TSM_DB_SIZE 100 \
	"<n>" "size of TSM database"

defconf TSM_SPACE_MGMT_SIZE 1024 \
	"<n>" "size of TSM space management pool"

defconf TSM_BACKUP_POOL_SIZE 100 \
	"<n>" "size of TSM backup pool"

defconf TSM_ARCHIVE_POOL_SIZE 100 \
	"<n>" "size of TSM archive pool"

defconf TSM_TEMPLATE "$installdir/templates/tsmserver.xml" \
	"<file>" "libvirt template for TSM server"

##############################

# TSM server node type

node_name_format_tsm ()
{
    local cluster="$1"
    local index="$2"

    # If only 1 tsm_server node then don't number it.
    if [ "${NODES/tsm_server:/}" = "${NODES//tsm_server:/}" ] ; then
	printf '%stsm' "$cluster"
    else
	printf '%stsm%d' "$cluster" "$index"
    fi
}

create_node_tsm_server ()
{
    local ip_offset="$1"
    local name="$2"
    local ctdb_node="$3"

    # This must be before defined create_node_COMMON because the
    # template needs the value of TSMDISK.
    TSMDISK="${VIRTBASE}/${CLUSTER}/${name}storage.qcow2"

    echo "Creating node \"${name}\" (of type \"tsm_server\""
    create_node_COMMON "$name" "$ip_offset" "$type" "$TSM_TEMPLATE"

    local di="$TSMDISK"
    if [ "$DISK_FOLLOW_SYMLINKS" = "yes" -a -L "$TSMDISK" ] ; then
	di=$(readlink "$TSMDISK")
    fi

    echo "Creating TSM disk"
    qemu-img create -f qcow2 "$di" $TSMDISKSIZE

    echo
}
