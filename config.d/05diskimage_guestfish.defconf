# Hey Emacs, this is a -*- shell-script -*- !!!

# This file provides functions to implement access/update of disk
# images via guestfish.

######################################################################

diskimage_mount_guestfish ()
{
    echo "Using guestfish to update disk image ${disk}..."

    local mount_args=""
    local m d
    for m in $SYSTEM_DISK_MOUNTS ; do
	d="${m#*:}" # Device is after colon
	m="${m%:*}" # Mountpoint is before colon
	mount_args="${mount_args}${mount_args:+ } --mount ${d}:${m}"
	echo " mount ${m} from device ${d} configured"
    done
    [ -n "$mount_args" ] || mount_args="-i"

    eval $(guestfish --listen -a "$1" $mount_args)

    guestfish --remote ping-daemon || \
	die "Failed to initialise guestfish session"

    diskimage is_directory "/root" || {
	echo "Mounted directory does not look like a root filesystem"
	guestfish --remote ll /
	exit 1
    }

    echo $GUESTFISH_PID > tmp/guestfish.pid
    echo "To attach to guestfish image"
    echo "  export GUESTFISH_PID=$GUESTFISH_PID"
    echo "  guestfish --remote" 
}

# unmount a qemu image
diskimage_unmount_guestfish ()
{
    read GUESTFISH_PID < tmp/guestfish.pid
    export GUESTFISH_PID

    echo "Unmounting disk"
    guestfish --remote sync
    guestfish --remote exit
}

diskimage_mkdir_p_guestfish ()
{
    local t=$(guestfish --remote is-dir "$1/.")
    if [ "$t" = "false" ] ; then
	guestfish --remote mkdir-p "$1"
    fi
}

diskimage_substitute_vars_guestfish ()
{
    local t=$(mktemp)
    substitute_vars "$1" "$t"
    guestfish --remote upload "$t" "$2"
    rm "$t"
}

diskimage_chmod_guestfish ()
{
    local mode="$1" ; shift

    # For guestfish, octal mode must start with '0'.
    case "$mode" in
	(0*) : ;;
	(*) mode="0${mode}" ;;
    esac

    local i
    for i ; do
	guestfish --remote glob chmod "$mode" "$i"
    done
}

diskimage_chmod_reference_guestfish ()
{
    local mode=$(printf "%o\n" $(( 0x$(stat -c "%f" "$1") )) )
    mode="0${mode: -3}"
    shift

    local i
    for i ; do
	guestfish --remote glob chmod "$mode" "$i"
    done
}

diskimage_is_file_guestfish ()
{
    local t=$(guestfish --remote is-file "$1")
    [ "$t" = "true" ]
}

diskimage_is_directory_guestfish ()
{
    local t=$(guestfish --remote is-dir "$1/.")
    [ "$t" = "true" ]
}

diskimage_append_text_file_guestfish ()
{
    local t=$(mktemp)
    guestfish --remote download "$2" "$t"
    cat "$1" >> "$t"
    guestfish --remote upload "$t" "$2"
    rm "$t"
}

diskimage_append_text_guestfish ()
{
    local t=$(mktemp)
    guestfish --remote download "$2" "$t"
    echo "$1" >> "$t"
    guestfish --remote upload "$t" "$2"
    rm "$t"
}

diskimage_sed_guestfish ()
{
    local file="$1" ; shift

    local t=$(mktemp)
    guestfish --remote download "$file" "$t"
    sed -i "$@" "$t"
    guestfish --remote upload "$t" "$file"
    rm "$t"
}

diskimage_grep_guestfish ()
{
    local file="$1" ; shift

    # guestfish's grep doesn't support options like -f, so don't use it.
    local ret
    local t=$(mktemp)
    guestfish --remote download "$file" "$t"
    grep "$@" "$t"
    ret=$?
    rm "$t"

    return $ret
}

diskimage_put_guestfish ()
{
    if [ "$1" = "-" ] ; then
	local t=$(mktemp)
	cat > "$t"
	guestfish --remote upload "$t" "$2"
	rm "$t"
    else
	guestfish --remote upload "$1" "$2"
    fi
}

diskimage_ln_s_guestfish ()
{
    guestfish --remote ln-s "$1" "$2"
}

diskimage_command_guestfish ()
{
    # Yes, I mean "$*" and not "$@".  The command passed must be a
    # single string...  and, yes, quoting is lost.
    guestfish --remote command "$*"
}

diskimage_mv_guestfish ()
{
    guestfish --remote mv "$1" "$2"
}

diskimage_rm_rf_guestfish ()
{
    guestfish --remote rm-rf "$1"
}

######################################################################

diskimage_guestfish_sanity_check ()
{
    if [ "$SYSTEM_DISK_ACCESS_METHOD" = "guestfish" ] ; then
	check_command guestfish
    fi
}

register_hook post_config_hooks diskimage_guestfish_sanity_check
